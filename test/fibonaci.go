package test

import (
	"fmt"
	"strconv"
)

// Fibonaci -- Fungsi fibonaci
func Fibonaci(jumlah int) string {
	var angkaSebelumnya int = 0
	var angkaSekarang int = 1

	var hasil string = strconv.Itoa(angkaSekarang)

	var i int

	for i = 0; i < jumlah-1; i++ {
		var output int = angkaSekarang + angkaSebelumnya
		hasil = hasil + strconv.Itoa(output)
		angkaSebelumnya = angkaSekarang
		angkaSekarang = output
	}
	return hasil
}

// PiramidaFibonaci -- piramida fibonaci
func PiramidaFibonaci(tingkat int) {
	var i int
	for i = 1; i < tingkat+1; i++ {
		fmt.Printf("%s\n", Fibonaci(i))
	}
}
