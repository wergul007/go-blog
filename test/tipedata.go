package test

import (
	"fmt"
)

// contoh array
var dataArr = []string{
	"Januari",
	"Febuari",
	"Maret",
	"April",
	"Mei",
	"Juni",
	"Juli",
	"Agustus",
	"September",
	"Oktober",
	"November",
	"Desember"}

// contoh multidimensi array
var dataArrTgl = [][]int{
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
	[]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30}}

// contoh data map
var dataMap = map[int]string{
	1:  "January",
	2:  "Febuari",
	3:  "Maret",
	4:  "April",
	5:  "Mei",
	6:  "Juni",
	7:  "Juli",
	8:  "Agustus",
	9:  "September",
	10: "Oktober",
	11: "November",
	12: "Desember"}

type dataStruct struct {
	Nama string
	Tgl  int
}

// TipeDataArr -- menampilkan data array
func TipeDataArr(pakeLoop bool) {
	if pakeLoop == true {
		var i int
		for i = 0; i < len(dataArr); i++ {
			fmt.Printf("%s bulan ke %d\n", dataArr[i], i+1)
		}
	} else {
		fmt.Println(dataArr)
	}
}

// TipeDataArrMultiDimensi -- menampilkan data array multidimensi
func TipeDataArrMultiDimensi(pakeLoop bool) {

	if pakeLoop == true {
		var i int
		for i = 0; i < len(dataArrTgl); i++ {
			fmt.Printf("%d bulan ke %d adalah bulan %s ada %d hari\n", dataArrTgl[i], i+1, dataArr[i], len(dataArrTgl[i]))
		}
	} else {
		fmt.Println(dataArrTgl)
	}
}

// TipeDataMap -- menampilkan data map
func TipeDataMap(modified bool) {

	if modified == true {
		for k, v := range dataMap {
			fmt.Printf("Bulan %s adalah bulan ke %d \n", v, k)
		}
	} else {
		var i int
		for i = 0; i < len(dataMap); i++ {
			fmt.Printf("Bulan %s adalah bulan ke %d dengan index %d\n", dataMap[i+1], i+1, i)
		}
	}
}

// TipeDataStruct -- menampilkan data struct
func TipeDataStruct() {
	var i int
	var items []dataStruct
	for i = 0; i < len(dataArr); i++ {
		item := dataStruct{Nama: dataArr[i], Tgl: len(dataArrTgl[i])}
		items = append(items, item)
	}
	fmt.Println(items)
}
