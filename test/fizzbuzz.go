package test

import (
	"fmt"
)

// Fizzbuzz -- Fungsi fizzbuzz
func Fizzbuzz(mod int, howmany int) {
	var i int
	for i = 0; i < howmany; i++ {
		if (i % mod) == 0 {
			fmt.Printf("%s => %d\n", "Fizz", i)
		} else {
			fmt.Printf("%s => %d\n", "Buzz", i)
		}
	}
}
